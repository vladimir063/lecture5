package org.sber.exercise3;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Cat {

    @Min(value = 2)
    int age;

    @Max(value = 20)
    int height;

    @MinLength(value = 3)
    String name;

    @NotNull
    String color;

    @NotEmpty
    String breed;

}
