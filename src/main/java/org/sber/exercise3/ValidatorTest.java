package org.sber.exercise3;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class ValidatorTest {


    @Test
    public void validate() throws IllegalAccessException {
        Cat cat = new Cat(1, 30, "Li", null, "");
        List<String> validateList = Validator.validate(cat);
        assertEquals(validateList.size(), 5);
    }
}