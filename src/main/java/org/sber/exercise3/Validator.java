package org.sber.exercise3;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class Validator {

    static List<String> validateResult = new ArrayList<>();

    static List<String> validate(Object object) throws IllegalAccessException {
        Field[] declaredFields = object.getClass().getDeclaredFields();
        for (Field field : declaredFields) {
            if (field.isAnnotationPresent(Min.class)) {
                minValidator(object, field);
            } else if (field.isAnnotationPresent(Max.class)) {
                maxValidator(object, field);
            } else if (field.isAnnotationPresent(MinLength.class)) {
                minLengthValidator(object, field);
            } else if (field.isAnnotationPresent(NotNull.class)) {
                notNullValidator(object, field);
            } else if (field.isAnnotationPresent(NotEmpty.class)) {
                notEmptyValidator(object, field);
            }
        }
        return validateResult;
    }

    private static void notEmptyValidator(Object object, Field field) throws IllegalAccessException {
        String str = (String) field.get(object);
        if (str.isEmpty()) {
            validateResult.add("Поле " + field.getName() + " пустое");
        }

    }

    private static void notNullValidator(Object object, Field field) throws IllegalAccessException {
        if (field.get(object) == null) {
            validateResult.add("Поле " + field.getName() + " равно " + null);
        }
    }

    private static void minLengthValidator(Object object, Field field) throws IllegalAccessException {
        MinLength annotation = field.getAnnotation(MinLength.class);
        int min = annotation.value();
        String str = (String) field.get(object);
        if (str.length() < min) {
            validateResult.add("Длина строки " + field.getName() + " меньше, чем " + min);
        }
    }

    private static void minValidator(Object object, Field field) throws IllegalAccessException {
        Min annotation = field.getAnnotation(Min.class);
        int min = annotation.value();
        int objectMin = field.getInt(object);
        if (objectMin < min) {
            validateResult.add("Поле " + field.getName() + " меньше, чем " + min);
        }
    }

    private static void maxValidator(Object object, Field field) throws IllegalAccessException {
        Max annotation = field.getAnnotation(Max.class);
        int max = annotation.value();
        int objectMax = field.getInt(object);
        if (objectMax > max) {
            validateResult.add("Поле " + field.getName() + " больше, чем " + max);
        }
    }

}
