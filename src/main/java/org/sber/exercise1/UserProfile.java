package org.sber.exercise1;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class UserProfile {

    private String firstName;
    private String lastName;
    private String password;
    private String series;
    private String number;
    private LocalDate date;


    @Override
    public String toString() {
        return "UserProfile{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", password='" + password + '\'' +
                ", series='" + series + '\'' +
                ", number='" + number + '\'' +
                ", date=" + date +
                '}';
    }
}
