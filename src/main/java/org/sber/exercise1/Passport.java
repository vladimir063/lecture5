package org.sber.exercise1;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class Passport {

    private String series;
    private String number;
    private LocalDate date;

    public Passport(String series, String number, LocalDate date) {
        this.series = series;
        this.number = number;
        this.date = date;
    }
}
