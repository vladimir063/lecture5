package org.sber.exercise1;

import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;

public class ObjectCreaterTest {


    @Test
    public void mergeObjects() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        Person person = new Person("Ivan", "Ivanov", "qwerty");
        Passport passport = new Passport("2323", "2424242", LocalDate.now());
        UserProfile userProfile = ObjectCreater.mergeObjects(person, passport, UserProfile.class);

        Assert.assertEquals(userProfile.getFirstName(), person.getFirstName());
        Assert.assertEquals(userProfile.getLastName(), person.getLastName());
        Assert.assertEquals(userProfile.getPassword(), person.getPassword());
        Assert.assertEquals(userProfile.getSeries(), passport.getSeries());
        Assert.assertEquals(userProfile.getNumber(), passport.getNumber());
        Assert.assertEquals(userProfile.getDate(), passport.getDate());
    }
}