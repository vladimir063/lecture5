package org.sber.exercise1;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class ObjectCreater {

    public static UserProfile mergeObjects(Person person, Passport passport, Class clazz) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, InstantiationException {
        Field[] personFields = person.getClass().getDeclaredFields();
        Field[] declaredFields = passport.getClass().getDeclaredFields();
        Field[] clazzDeclaredFields = clazz.getDeclaredFields();


        UserProfile userProfile = (UserProfile) clazz.getConstructor().newInstance();
        //     Field[] userProfileFields = userProfile.getClass().getDeclaredFields();
        for (Field userProfileField : clazzDeclaredFields) {
            userProfileField.setAccessible(true);
            for (Field personField : personFields) {
                personField.setAccessible(true);
                if (personField.getName().equals(userProfileField.getName())) {
                    userProfileField.set(userProfile, personField.get(person));

                }
            }
            for (Field declaredField : declaredFields) {
                declaredField.setAccessible(true);
                if (declaredField.getName().equals(userProfileField.getName())) {
                    userProfileField.set(userProfile, declaredField.get(passport));

                }
            }
        }

        return userProfile;
    }
}
