package org.sber.exercise2;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;

public class MethodInfo {

    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();
        map.put("aaa", 2);
        map.put("vvv", 3);
        map.put("ccc", 4);

        CustomInvocationHandler customInvocationHandler = new CustomInvocationHandler(map);
        Class[] classes = {Map.class};
        Map mapProxy = (Map) Proxy.newProxyInstance(Map.class.getClassLoader(), classes, customInvocationHandler);
        mapProxy.size();

    }

    static class CustomInvocationHandler implements InvocationHandler {

        private Map map;

        public CustomInvocationHandler(Map map) {
            this.map = map;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            System.out.println("Имя метода - " + method.getName());
            LocalTime start = LocalTime.now();
            method.invoke(map);
            LocalTime finish = LocalTime.now();
            System.out.println("Время выполнения метода - " + ChronoUnit.NANOS.between(start, finish));
            return null;
        }
    }
}
